const test = require('blue-tape')
const { ephemeral } = require('..')
const { createServer, connect } = require('tls')

let credentials
test('Get key & certificate', async (t) => {
  credentials = await ephemeral({
    entrust: false,
    commonName: 'example.net',
    subjectAltName: [
      'DNS:example.net',
      'DNS:*.example.net'
    ]
  })
})

let server
test('Start TLS server', (t) => {
  server = createServer(credentials)
  server.listen(t.end)
})

test('Connect TLS client', (t) => {
  const client = connect(server.address().port, {
    ca: [credentials.cert],
    checkServerIdentity: function (servername, cert) {
      t.is(cert.subject.CN, 'example.net')
      t.is(cert.subjectaltname, 'DNS:example.net, DNS:*.example.net')
    }
  })
  client.on('secureConnect', () => {
    client.end(t.end)
  })
})

test('Stop TLS server', (t) => server.close(t.end))
