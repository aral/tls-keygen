const test = require('blue-tape')
const { ephemeral } = require('..')

test('Ephmeral key/cert', async (t) => {
  const { key, cert } = await ephemeral({ entrust: false })
  t.ok(key)
  t.ok(cert)
})
